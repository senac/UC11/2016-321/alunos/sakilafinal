

<jsp:include page="../header.jsp" />




<div class="container">

    <fieldset>
        <legend>Cadastro de Pa�ses</legend>
    </fieldset>

    <form  class="form-horizontal" method="post" action="./pais.do">

        <jsp:useBean id="pais"  class="br.com.senac.modelo.Pais" scope="session">
            <jsp:setProperty name="pais" property="*" />
        </jsp:useBean>

        <div class="form-group">
            
            <input type="hidden" value="${pais.codigo}" name="codigo" />
            
            <label class="control-label col-sm-2 col-xs-12" for="codigo">C�digo:</label>
            <div class="col-sm-2 col-xs-12">
                <input type="text" class="form-control" readonly="true" id="codigo"  value="${pais.codigo == 0 ? "" : pais.codigo }" />
            </div>

        </div>
        <div class="form-group">
            <label class="control-label col-sm-2 col-xs-12" for="pais">Pa�s:</label>
            <div class="col-sm-4 col-xs-12">
                <input type="text" class="form-control" id="pais" name="nome" value="${pais.nome}" />
            </div>
            <div class="col-sm-6 col-xs-12">
            </div>

        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary col-xs-12 col-sm-2" value="Salvar" />
                <input type="reset" class="btn btn-danger col-xs-12 col-sm-2" value="Cancelar" />
            </div>

        </div>


    </form>



</div>


<jsp:include page="../footer.jsp" />