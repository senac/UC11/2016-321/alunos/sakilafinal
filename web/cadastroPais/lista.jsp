<%@page import="java.util.List"%>
<%@page import="br.com.senac.modelo.Pais"%>
<%@page import="br.com.senac.banco.PaisDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:include page="../header.jsp" />




<%
    PaisDAO dao = new PaisDAO();
    List<Pais> lista = dao.listarTodos();
%>

<div class="container">
    <fieldset>
        <legend>Lista de Países</legend>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>País</th>
                </tr>
            </thead>
            <tbody>


                <% for (Pais p : lista) { %>

                <tr>
                    <td><% out.print(p.getCodigo()); %></td>
                    <td><% out.print(p.getNome()); %></td>
                </tr>

                <% }%>

            </tbody>
        </table>

    </fieldset>

</div>

<jsp:include page="../footer.jsp" />






















